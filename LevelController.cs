﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;
using Vuforia;
using Random = UnityEngine.Random;

public class LevelController : NetworkBehaviour {

	//public GameObject PlateConstructionsPrefab;
	public GameObject PlateConstructions;
	public GameObject PlayerUnitPrefab;
	public GameObject Plate;
	public GameObject PlayerUnitConnection;

	NetworkManager networkManager = NetworkManager.singleton;
	private float halfSizePlayer;
	private float plateRadius;
	[SyncVar]
	private GameObject Player1;
	[SyncVar]
	private GameObject Player2;
	private GameObject BotPlayer;

	public override void OnStartServer()
	{
		if(!isServer)
		{
			return;
		}
		
		StartCoroutine(WaitUntilPlayerInitializade("Player1"));
		StartCoroutine(WaitUntilSecondPlayerConnected());

		CmdInitPlateConstruction();
	}
	
	void Start()
	{
		DefaultTrackableEventHandler.OnTrackFound += TrackFound;
		DefaultTrackableEventHandler.OnTrackLost += TrackLost;
	}

	void OnDestroy()
	{
		DefaultTrackableEventHandler.OnTrackFound -= TrackFound;
		DefaultTrackableEventHandler.OnTrackLost -= TrackLost;
	}

	private void TrackFound(object sender, EventArgs e)
	{
		Debug.Log(Plate.transform.GetComponent<MeshCollider>().bounds.size);
		plateRadius = (Plate.transform.GetComponent<MeshCollider>().bounds.size.x / 2/* / 27 * 12*/) - halfSizePlayer;

		CmdSetPlayersActive(false);
	}

	private void TrackLost(object sender, EventArgs e)
	{
		CmdSetPlayersActive(false);
	}
	
	void Update ()
	{
	}

	//[Command]
	void CmdInitPlateConstruction()
	{
		//NetworkServer.SpawnWithClientAuthority(Instantiate(PlateConstructionsPrefab), connectionToClient);
		PlateConstructions.SetActive(true);
	}

	void ReneratePostitionOfPLayers()
	{
		List<Vector3> posList = new List<Vector3>();
		Vector3 player1Pos = GeneratePosition();
		posList.Add(player1Pos);

		Vector3 player2Pos = GeneratePositionExcludeListPos(posList);
		posList.Add(player2Pos);

		Vector3 botPos = GeneratePositionExcludeListPos(posList);
		SetUpPLayers();
		Player1.transform.localPosition = player1Pos;
		Player2.transform.localPosition = player2Pos;

		CmdSetPlayersActive(true);
		//GlobalPlayerControl.BotPlayer.transform.localPosition = botPos;

		//Plate.trnsform.rotation = new Quaternion(0, 0, 0, 0);
	}
	Vector3 GeneratePosition()
	{
		return new Vector3(Random.Range(-plateRadius, plateRadius), Player1.transform.position.y, Random.Range(-plateRadius, plateRadius));
	}

	Vector3 GeneratePositionExcludeListPos(List<Vector3> excludeList)
	{
		Vector3 playerPos = GeneratePosition();
		// TODO :: Избавиться от вечного цикла, правильно высчитывать halfSizePlayer
		/*excludeList.ForEach(a =>
		{
			while (Vector3.Distance(a, playerPos) <= halfSizePlayer)
			{
				playerPos = GeneratePosition();
			}
		});*/

		return playerPos;
	}

	void SetUpPlayer(GameObject Player)
	{
		Player.transform.rotation = new Quaternion(0, 0, 0, 0);
		Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
	}

	void SetUpPLayers()
	{
		SetUpPlayer(Player1);
		SetUpPlayer(Player2);
		//SetUpPlayer(BotPlayer);
	}

	IEnumerator WaitUntilSecondPlayerConnected()
	{
		yield return new WaitUntil(() => NetworkServer.connections.Count == 2 && GameObject.Find("Player2") != null);
		Player2 = GameObject.FindWithTag("Player2");
		Player2.SetActive(false);
		ReneratePostitionOfPLayers();
	}

	IEnumerator WaitUntilPlayerInitializade(string playerName)
	{
		yield return new WaitUntil(() => GameObject.Find(playerName) != null);
		Player1 = GameObject.FindWithTag(playerName);
		Debug.Log(Player1.GetComponentInChildren<MeshCollider>().bounds.size);
		halfSizePlayer = Player1.GetComponentInChildren<MeshCollider>().bounds.size.x / 2/* / 27 * 20*/;
		Player1.SetActive(false);
	}

	[Command]
	void CmdSetPlayersActive(bool setting)
	{
		if (Player1 != null)
		{
			Player1.SetActive(setting);
		}
		if (Player2 != null)
		{
			Player2.SetActive(setting);
		}
	}
}
