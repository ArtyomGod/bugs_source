﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager Instance { get; set; }

	void Start () {
		GameObject.Find("DisconnectButton").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("DisconnectButton").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
	}
}
