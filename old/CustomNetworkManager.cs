﻿using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class CustomNetworkManager : NetworkManager
{

	public void StartupHost()
	{
		SetPort();
		SetMatchName();
		singleton.StartHost();
	}

	public void StartupClient()
	{
		SetPort();
		SetMatchName();
		singleton.StartClient();
	}

	public void SetMatchName()
	{
		singleton.networkAddress = "localhost";
	}

	public void SetPort()
	{
		singleton.networkPort = 7777;
	}

	public void CreateMatch()
	{
		singleton.StartMatchMaker();
		singleton.matchMaker.CreateMatch("game", 2, true, "", "", "", 0, 0, OnMatchCreate);
	}
	public void JoinMatch()
	{
		matches.FirstOrDefault();
		MatchInfoSnapshot matchInfo = singleton.matches.First();
		singleton.StartMatchMaker();
		singleton.matchMaker.JoinMatch(matchInfo.networkId, "", "", "", 0, 0, OnMatchJoined);
	}
}
