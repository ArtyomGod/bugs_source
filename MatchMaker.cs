﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class MatchMaker : NetworkManager
{
	void Start()
	{
		Debug.Log("BUGS :: Start MachMaker");
		NetworkManager.singleton.StartMatchMaker();
		StartUpMenuScenceButtons();
	}

	void Update()
	{
	}

	public void CreateInternetMatch()
	{
		NetworkManager.singleton.matchMaker.CreateMatch("game", 2, true, "", "", "", 0, 0, OnInternetMatchCreate);
	}

	private void OnInternetMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
	{
		if (success)
		{
			MatchInfo hostInfo = matchInfo;
			NetworkServer.Listen(hostInfo, 7777);
			NetworkManager.singleton.StartHost(hostInfo);
			//зпускем клиента 
			//ждем что будет два подключения 
			//зпускаем уровнеь
			//StartCoroutine(WaitUntilSecondPlayerConnected());
		}
		else
		{
			Debug.LogError("Create match failed");
		}
	}

	public void FindInternetMatch()
	{
		NetworkManager.singleton.matchMaker.ListMatches(0, 10, "game", true, 0, 0, OnInternetMatchList);
	}

	private void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		if (success)
		{
			Debug.Log(matches.Count);
			if (matches.Count != 0)
			{
				DebugPrintAllGames(matches);
				NetworkManager.singleton.matchMaker.JoinMatch(matches.Last().networkId, "", "", "", 0, 0, OnJoinInternetMatch);
			}
			else
			{
				Debug.Log("No matches in requested room!");
			}
		}
		else
		{
			Debug.LogError("Couldn't connect to match maker");
		}
	}

	//this method is called when your request to join a match is returned
	private void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo)
	{
		if (success)
		{
			MatchInfo hostInfo = matchInfo;
			NetworkManager.singleton.StartClient(hostInfo);
		}
		else
		{
			Debug.LogError("Join match failed");
		}
	}
	private void DebugPrintAllGames(List<MatchInfoSnapshot> matches)
	{
		Debug.Log("Matches :: ");
		foreach (MatchInfoSnapshot match in matches)
		{
			Debug.Log(match.name + ": " + match.networkId);
		}
	}

	void StartUpMenuScenceButtons()
	{
		Debug.Log("BUGS :: SetUp Buttons");
		GameObject.Find("StartButton").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("StartButton").GetComponent<Button>().onClick.AddListener(CreateInternetMatch);

		GameObject.Find("ClientButton").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("ClientButton").GetComponent<Button>().onClick.AddListener(FindInternetMatch);
	}

	public override void OnStopHost()
	{
		StartCoroutine(a());
	}

	public IEnumerator a()
	{
		yield return new WaitWhile(() => GameObject.Find("StartButton") == null);
		Debug.Log("StartButton found" + GameObject.Find("StartButton").name);
		NetworkManager.singleton.StartMatchMaker();
		StartUpMenuScenceButtons();
	}
}