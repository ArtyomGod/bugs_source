﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Plate : MonoBehaviour
{
	private bool isBalancing = false;
	private float balancedSeconds = 0;
	
	void Start () {
		
	}
	
	void Update () {
		CheckPlateAngle();
	}

	void CheckPlateAngle()
	{
		var plateRotation = this.transform.rotation.eulerAngles;
		if (CheckAngle(plateRotation.x) && CheckAngle(plateRotation.z))
		{
			if (balancedSeconds == 0 && !isBalancing)
			{
				StartCoroutine(WaitUntilBalanced());
			}
			balancedSeconds++;
		}
		else
		{
			balancedSeconds = 0;
		}
	}
	IEnumerator WaitUntilBalanced()
	{
		isBalancing = true;
		yield return new WaitUntil(() => balancedSeconds >= 100);
		StopCoroutine(WaitUntilBalanced());
		isBalancing = false;
		Debug.Log("WIN");
		//SceneManager.LoadScene("WinMenu", LoadSceneMode.Additive);
		//SceneManager.UnloadScene("level");
	}
	bool CheckAngle(float angle)
	{
		angle = angle < 0 ? angle * -1 : angle;
		return angle < 4f && angle >= 0;
	}
}
