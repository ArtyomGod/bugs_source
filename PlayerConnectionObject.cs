﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerConnectionObject : NetworkBehaviour {

	public GameObject PlayerUnitPrefab;

	void Start () {
		if(!isLocalPlayer)
		{
			return;
		}
		CmdSpawnUnit();
	}

	void Update () {
	}

	[Command]
	void CmdSpawnUnit()
	{
		NetworkServer.SpawnWithClientAuthority(Instantiate(PlayerUnitPrefab), connectionToClient);
	}
}
