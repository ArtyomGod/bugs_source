﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerUnit : NetworkBehaviour {

	public GameObject plate;

	Vector3 plateCenter;
	float plateRadius;
	float halfSizePlayer;	

	void Start () {
		Debug.Log("CREATING PLAYER NAME! COUNT OF PLAYERS = " + NetworkServer.connections.Count);
		transform.parent = GameObject.Find("ImageTarget").transform;
		tag = name = NetworkServer.connections.Count == 1 ? "Player1" : "Player2";


		plateCenter = new Vector3(0, plate.GetComponent<MeshRenderer>().bounds.center.y + 1f, 0);

		halfSizePlayer = this.GetComponentInChildren<MeshCollider>().bounds.size.x / 27 * 20;
		plateRadius = (plate.transform.GetComponent<MeshCollider>().bounds.size.x / 27 * 12) - halfSizePlayer;
		FreezePozition();
	}
	
	void Update () {
		
		if (!hasAuthority)
		{
			return;
		}
		UnfreezePozition();
		MovePLayer();		
	}

	void FreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX;
	}

	void UnfreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionX | ~RigidbodyConstraints.FreezePositionZ;
	}

	void MovePLayer()
	{
		float x = CrossPlatformInputManager.GetAxis("Horizontal");
		float z = CrossPlatformInputManager.GetAxis("Vertical");

		if (Math.Abs(x) < 0 && Math.Abs(z) < 0)
		{			
			FreezePozition();
		}

		Vector3 movement = new Vector3(x, 0, z);
		Vector3 nextPos = this.transform.position + movement * Time.deltaTime;

		if (x != 0 && z != 0)
		{
			this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, Mathf.Atan2(x, z) * Mathf.Rad2Deg,
				this.transform.eulerAngles.z);
		}

		//TODO:: make ограничения для передвижения - радиус тарелки
		
		/*if (Vector3.Distance(plateCenter, nextPos) < plateRadius)
		{
			this.transform.position = nextPos;
		}*/

		this.transform.position = nextPos;
	}
	void OnCollisionEnter(Collision collision)
	{
		
		if (collision.gameObject.name == "Player1" || collision.gameObject.name == "Player2" || collision.gameObject.name == "BotPlayer")
		{
			Physics.IgnoreCollision(collision.collider, gameObject.GetComponentInChildren<MeshCollider>());
		}

	}
}
