﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour {

	public Button BackToMenuButton;

	// Use this for initialization
	void Start () {
		BackToMenuButton.onClick.AddListener(StopHost);
	}
	public static void StopHost()
	{
		NetworkManager.singleton.StopHost();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
